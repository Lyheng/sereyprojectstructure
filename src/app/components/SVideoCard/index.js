import React, { Component } from "react";
import { Col, Row } from "antd";
import SImageBackground from "../SImageBackground";
class SVideoCard extends Component {
  render() {
    let { size } = this.props;
    return (
      <div
        style={{
          height: 200,
          backgroundColor: "green",
          marginTop: 25,
          width: "30%"
        }}
      >
        <SImageBackground
          imgUrl="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRD0woMBCaO7NUHw_S1D1zJwJhssyPFlSsUZWoDdTj4OnkmNuCe2Q"
          style={{
            padding: "0px 20px 20px 20px",
            fontWeight: "bold",
            color: "black",
            justifyContent: "space-between"
          }}
        >
          <div
            style={{
              backgroundColor: "#FFAF31",
              width: "30%",
              textAlign: "center",
              color: "white"
            }}
          >
            BITCOIN
          </div>
          Tourism in Dubai is booming international tourist most visited place
        </SImageBackground>
      </div>
    );
  }
}

export default SVideoCard;
