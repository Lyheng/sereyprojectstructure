import React, { Component } from "react";
import { Card, Row, Col, Avatar, Icon } from "antd";
import { Clock } from "../Icon";

const { Meta } = Card;
class SCard extends Component {
  static defaultProps = {
    imgUrl: "https://ichef.bbci.co.uk/images/ic/720x405/p0517py6.jpg"
  };
  render() {
    let { imgUrl } = this.props;
    return (
      <div style={{ height: 350, width: "100%" }}>
        <div
          style={{
            width: "100%",
            height: "65%",

            backgroundImage: `url(${imgUrl})`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "100% 100%",
            backgroundPosition: "center"
          }}
        >
          <Row>
            <Col span={2} />
            <Col
              span={5}
              style={{
                backgroundColor: "#FFAF31",
                height: 20
              }}
            >
              <p style={{ color: "white", fontSize: 12, textAlign: "center" }}>
                BITCOIN
              </p>
            </Col>
          </Row>
        </div>
        <Row>
          <Col>
            <Row
              style={{
                position: "relative",
                width: "50%",
                bottom: 17,
                backgroundColor: "white",
                alignItems: "center"
              }}
              type="flex"
              align="middle"
            >
              <Col span={6}>
                <Avatar icon="user" />
              </Col>
              <Col span={18}>Rin Sethneary</Col>
            </Row>
          </Col>
        </Row>
        <Row>
          <Col style={{ fontWeight: "bold", fontSize: 15 }}>
            Bitcoin Extends Range While Ethereum Hits New Yearly Low
          </Col>
        </Row>

        <div
          style={{
            display: "flex",
            flexDirection: "row",

            alignItems: "flex-end",
            height: 30
          }}
        >
          <Icon
            component={Clock}
            width="1em"
            height="1em"
            style={{ color: "grey" }}
          />
          <div style={{ fontSize: 10, marginLeft: 2, color: "grey" }}>
            March 21, 2019
          </div>
        </div>
      </div>
    );
  }
}

export default SCard;
