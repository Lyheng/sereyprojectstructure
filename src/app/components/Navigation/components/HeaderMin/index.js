import React, { Component } from "react";

import {
  Menu,
  Icon,
  Avatar,
  message,
  Dropdown,
  Button,
  Input,
  Row,
  Col,
  Divider,
  Badge
} from "antd";
import { Hamburger } from "../../../Icon";
const Search = Input.Search;
class HeaderMin extends Component {
  menu = (
    <Menu onClick={this.handleMenuClick}>
      <Menu.Item key="1">Khmer</Menu.Item>
    </Menu>
  );
  render() {
    return (
      <Row align="middle" type="flex">
        <Col span={15}>
          <img src={require("../../../../assets/serey-logo.png")} />
          <Dropdown overlay={this.menu}>
            <Button style={{ marginLeft: 8 }}>
              English <Icon type="down" />
            </Button>
          </Dropdown>
        </Col>

        <Col span={9}>
          <Row type="flex" align="middle" style={{ height: 60 }}>
            <Icon
              type="search"
              style={{ fontSize: 30, marginTop: 10, marginRight: 20 }}
            />

            <Button
              size="large"
              style={{
                backgroundColor: "#032EA1",
                color: "white",
                marginRight: 20,
                boxShadow: `7px 7px 0 #E00025`
              }}
            >
              Post
            </Button>
            <Row
              type="flex"
              justify="flex-start"
              align="middle"
              style={{ marginTop: 10 }}
            >
              <Avatar size="large" icon="user" style={{ marginRight: 20 }} />
              <div style={{ marginRight: 20 }}>
                <Badge count={9}>
                  <Icon type="bell" style={{ fontSize: 30 }} />
                </Badge>
              </div>

              <Icon
                component={Hamburger}
                style={{
                  fontSize: 20,
                  height: 50,
                  width: 40,
                  marginRight: 20
                }}
              />
            </Row>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default HeaderMin;
