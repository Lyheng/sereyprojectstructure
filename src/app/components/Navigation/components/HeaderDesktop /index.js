import React, { Component } from "react";
import {
  Menu,
  Icon,
  Avatar,
  message,
  Dropdown,
  Button,
  Input,
  Row,
  Col,
  Divider,
  Badge
} from "antd";
import { Hamburger } from "../../../Icon";
const Search = Input.Search;
export default class HeaderDesktop extends Component {
  menu = (
    <Menu onClick={this.handleMenuClick}>
      <Menu.Item key="1">Khmer</Menu.Item>
    </Menu>
  );
  render() {
    return (
      <Row align="middle" type="flex">
        <Col span={6}>
          <img src={require("../../../../assets/serey-logo.png")} />
          <Dropdown overlay={this.menu}>
            <Button style={{ marginLeft: 8 }}>
              English <Icon type="down" />
            </Button>
          </Dropdown>
        </Col>

        <Col span={12}>
          <Row>
            <Col span={4} />
            <Col span={16}>
              <Search
                placeholder="input search text"
                onSearch={value => console.log(value)}
                style={{ width: "100%" }}
              />
            </Col>
            <Col span={4} />
          </Row>
        </Col>
        <Col span={6}>
          <Row type="flex" align="middle" style={{ height: 60 }}>
            <Col style={{ marginRight: 20 }}>
              <Button
                size="large"
                style={{
                  backgroundColor: "#032EA1",
                  color: "white",
                  boxShadow: `7px 7px 0 #E00025`
                }}
              >
                Post
              </Button>
            </Col>
            <Col>
              <Row
                type="flex"
                justify="flex-start"
                align="middle"
                style={{ marginTop: 10 }}
              >
                <Col style={{ marginRight: 20 }}>
                  <Avatar size="large" icon="user" />
                </Col>
                <Col style={{ marginRight: 20 }}>
                  <Badge count={9}>
                    <Icon type="bell" style={{ fontSize: 30 }} />
                  </Badge>
                </Col>
                <Col style={{ marginRight: 20 }}>
                  <Icon
                    component={Hamburger}
                    style={{
                      fontSize: 20,
                      height: 50,
                      width: 40
                    }}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}
