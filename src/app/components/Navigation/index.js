import React, { Component } from "react";
import {
  Menu,
  Icon,
  Avatar,
  message,
  Dropdown,
  Button,
  Input,
  Row,
  Col,
  Divider,
  Badge
} from "antd";
import "./style.scss";

import Responsive from "react-responsive";
import HeaderDesktop from "./components/HeaderDesktop ";
import HeaderMin from "./components/HeaderMin/index";
import HeaderTablet from "./components/HeaderTablet";
import { Link } from "react-router-dom";
import HeaderMobile from "./components/HeaderMobile/index";
const Desktop = props => <Responsive {...props} minWidth={1310} />;
const Laptop = props => (
  <Responsive {...props} maxWidth={1310} minWidth={1200} />
);
const Tablet = props => (
  <Responsive {...props} maxWidth={1200} minWidth={767} />
);
const Mobile = props => <Responsive {...props} maxWidth={767} />;
const Default = props => <Responsive {...props} minWidth={768} />;

const Search = Input.Search;
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

const Header = () => {
  return (
    <>
      <Desktop>
        <HeaderDesktop />
      </Desktop>
      <Laptop>
        <HeaderMin />
      </Laptop>
      <Tablet>
        <HeaderTablet />
      </Tablet>
      <Mobile>
        <HeaderMobile />
      </Mobile>
    </>
  );
};
class SNavBar extends Component {
  state = {
    current: "mail"
  };

  handleMenuClick(e) {
    message.info("Click on menu item.");
    console.log("click", e);
  }
  menu = (
    <Menu onClick={this.handleMenuClick}>
      <Menu.Item key="1">Khmer</Menu.Item>
    </Menu>
  );

  render() {
    return (
      <Row>
        <Col span={2} />
        <Col span={20}>
          <Header />
        </Col>
        <Col>
          <Divider type="horizontal" />
        </Col>
        <Col>
          <Col span={6} />
          <Col span={12}>
            <Row align="middle" justify="center">
              <Col span={24}>
                <Menu
                  onClick={this.handleClick}
                  selectedKeys={[this.state.current]}
                  mode="horizontal"
                  style={{ borderWidth: 0 }}
                >
                  <Menu.Item key="mail" style={{ borderBottomWidth: 0 }}>
                    <Link to="/"> Home</Link>
                  </Menu.Item>
                  <Menu.Item key="tech" style={{ borderBottomWidth: 0 }}>
                    <Link to="/tech">Tech</Link>
                  </Menu.Item>
                  <Menu.Item key="culture" style={{ borderBottomWidth: 0 }}>
                    <Link to="/culture"> Culture</Link>
                  </Menu.Item>
                  <Menu.Item key="startups" style={{ borderBottomWidth: 0 }}>
                    <Link to="/startups"> Startups</Link>
                  </Menu.Item>
                  <Menu.Item key="health" style={{ borderBottomWidth: 0 }}>
                    <Link to="/health"> Health</Link>
                  </Menu.Item>
                  <Menu.Item key="science" style={{ borderBottomWidth: 0 }}>
                    <Link to="/science"> Science</Link>
                  </Menu.Item>
                  <Menu.Item key="popular" style={{ borderBottomWidth: 0 }}>
                    <Link to="/popular"> Popular</Link>
                  </Menu.Item>
                  <Menu.Item key="collections" style={{ borderBottomWidth: 0 }}>
                    <Link to="/collections"> Collections</Link>
                  </Menu.Item>
                  <Menu.Item key="more" style={{ borderBottomWidth: 0 }}>
                    <Link to="/more"> More</Link>
                  </Menu.Item>
                </Menu>
              </Col>
              <Col span={24}>
                <Col span={6} />
                <Col>
                  <Menu
                    onClick={this.handleClick}
                    selectedKeys={[this.state.current]}
                    mode="horizontal"
                    style={{ borderWidth: 0 }}
                  >
                    <Menu.Item
                      key="mail"
                      style={{ borderBottomWidth: 0, fontWeight: "bold" }}
                    >
                      Trending
                    </Menu.Item>
                    <Menu.Item
                      key="hot"
                      style={{ borderBottomWidth: 0, fontWeight: "bold" }}
                    >
                      Hot
                    </Menu.Item>
                    <Menu.Item
                      key="today"
                      style={{ borderBottomWidth: 0, fontWeight: "bold" }}
                    >
                      Today
                    </Menu.Item>
                    <Menu.Item
                      key="new"
                      style={{ borderBottomWidth: 0, fontWeight: "bold" }}
                    >
                      New
                    </Menu.Item>
                  </Menu>
                </Col>
              </Col>
            </Row>
          </Col>
        </Col>
      </Row>
    );
  }
}

export default SNavBar;
