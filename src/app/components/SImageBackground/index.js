import React, { Component } from "react";

class SImageBackground extends Component {
  render() {
    let { children, imgUrl, style } = this.props;
    return (
      <div
        style={{
          backgroundImage: `url(${imgUrl})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "100% 100%",
          height: "100%",
          width: "100%",
          justifyContent: "flex-end",
          display: "flex",
          flexDirection: "column",
          ...style
        }}
      >
        {children}
      </div>
    );
  }
}

export default SImageBackground;
