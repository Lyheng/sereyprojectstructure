import React, { Component } from "react";
import { Col, Row, Card, Avatar, Icon } from "antd";
import { Clock } from "../Icon";

class SCardBig extends Component {
  static defaultProps = {
    imgUrl:
      "https://images.boredomfiles.com/wp-content/uploads/sites/5/2017/05/banana-cat-1.png"
  };
  render() {
    let { imgUrl } = this.props;
    return (
      <Row gutter={16}>
        <Col style={{ paddingBottom: 20 }} md={24} xs={24} lg={12}>
          <div
            style={{
              backgroundImage: `url(${imgUrl})`,
              height: 350,
              backgroundRepeat: "no-repeat",
              backgroundSize: "100% 100%"
            }}
          />
        </Col>
        <Col style={{ padding: "50px 30px 30px 30px" }} md={24} xs={24} lg={12}>
          <div style={{ width: "95%" }}>
            <div
              style={{
                width: "15%",
                backgroundColor: "#FFAF31",
                justifyContent: "center",
                fontWeight: "bold",
                textAlign: "center",
                color: "white"
              }}
            >
              BITCOIN
            </div>
            <h2 style={{ fontWeight: "bold" }}>
              Clothing and Accessories for the Crypto Trader
            </h2>
            <div
              style={{
                display: "flex",
                width: "50%",
                alignItems: "center",
                justifyContent: "space-between",
                marginBottom: 10
              }}
            >
              <Avatar icon="user" />
              <div>Donald Ramos</div>
              <div style={{ display: "flex", alignItems: "center" }}>
                <Icon
                  component={Clock}
                  width="1em"
                  height="1em"
                  style={{ color: "grey" }}
                />
                <div style={{ fontSize: 10, marginLeft: 2, color: "grey" }}>
                  March 21, 2019
                </div>
              </div>
            </div>
            <p>
              Black farmers in the US's south faced with the continued failure
              their effort to run farm their lunched a lawsuit claiming white
              racism is to blame{" "}
            </p>
          </div>
        </Col>
      </Row>
    );
  }
}

export default SCardBig;
