import React, { Component } from "react";
import { Col, Row } from "antd";
class SContentContainer extends Component {
  render() {
    let { children, containerStyle, size } = this.props;
    return (
      <Row style={containerStyle}>
        <Col span={size} />
        {children}
        <Col span={size} />
      </Row>
    );
  }
}

export default SContentContainer;
