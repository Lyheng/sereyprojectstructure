import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import SNavBar from "../components/Navigation";
import Home from "./Home";

export default class AppRouter extends Component {
  render() {
    return (
      <Router>
        <>
          <SNavBar />
          <Route exact path="/" component={Home} />
        </>
      </Router>
    );
  }
}
