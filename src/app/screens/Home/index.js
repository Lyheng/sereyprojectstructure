import React, { Component } from "react";
import SNavBar from "../../components/Navigation";
import { Layout, Col, Row, Divider } from "antd";
import Responsive from "react-responsive";
import SCard from "../../components/SCard";
import SContentContainer from "../../components/SContentContainer/index";
import SCardBig from "../../components/SCardBig";
import SVideoCard from "../../components/SVideoCard";
let { Header, Footer, Sider, Content } = Layout;

const Desktop = props => <Responsive {...props} minWidth={992} />;
const Tablet = props => <Responsive {...props} minWidth={768} maxWidth={991} />;
const Mobile = props => <Responsive {...props} maxWidth={767} />;
const Default = props => <Responsive {...props} minWidth={768} />;
const adsUrl =
  "http://cdn.sabay.com/cdn/ads.sabay.com/images/fea6d0858c88a15a257aac78a33df582.gif";
const Example = () => (
  <div>
    <Desktop>Desktop or laptop</Desktop>
    <Tablet>Tablet</Tablet>
    <Mobile>Mobile</Mobile>
    <Default>Not mobile (desktop or laptop or tablet)</Default>
  </div>
);

class Home extends Component {
  render() {
    return (
      <SContentContainer size={3} containerStyle={{ marginTop: 10 }}>
        <Col span={18} style={{}}>
          <SCardBig />
          <Row gutter={30}>
            <Col span={8} md={12} xs={24} lg={8}>
              <SCard />
            </Col>
            <Col span={8} md={12} xs={24} lg={8}>
              <SCard />
            </Col>
            <Col span={8} md={12} xs={24} lg={8}>
              <SCard />
            </Col>
            <Col span={8} md={12} xs={24} lg={8}>
              <SCard />
            </Col>
            <Col span={8} md={12} xs={24} lg={8}>
              <SCard />
            </Col>
            <Col span={8} md={12} xs={24} lg={8}>
              <SCard />
            </Col>
          </Row>
          <Row />

          <Row>
            <Col span={23}>
              <div
                style={{
                  backgroundImage: `url(${adsUrl})`,
                  height: 100,
                  backgroundRepeat: "no-repeat",
                  backgroundSize: "100% 100%"
                }}
              />
            </Col>
          </Row>
          {/* Video section        */}
          <Row
            style={{
              backgroundColor: "#F7F7F7",
              marginTop: 20,
              paddingBottom: 20
            }}
          >
            <Col span={1} />
            <Col span={22}>
              <div
                style={{
                  display: "flex",
                  height: 50,

                  alignItems: "center"
                }}
              >
                <div
                  style={{
                    height: "50%",
                    backgroundColor: "#FFAF31",
                    width: 5,
                    marginRight: 20
                  }}
                />
                Watch now
                <div
                  style={{
                    height: 2,
                    width: "89.8%",
                    backgroundColor: "grey",
                    marginLeft: 20
                  }}
                />
              </div>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <SVideoCard size={7} />
                <SVideoCard size={7} />
                <SVideoCard size={7} />
              </div>
            </Col>
          </Row>
        </Col>
      </SContentContainer>
    );
  }
}

export default Home;
