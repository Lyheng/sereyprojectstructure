import React, { Component } from "react";

import "./App.css";
import { Button, Card } from "antd";
import Home from "./app/screens/Home";
import AppRouter from "./app/screens/Router";

const { Meta } = Card;

class App extends Component {
  render() {
    return <AppRouter />;
  }
}

export default App;
